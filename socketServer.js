let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let mongoose = require('mongoose');
let Message = require('./message');
let moment = require('moment');

let top_messages = 50;

mongoose.connect('mongodb+srv://chatapp:PvkFRc0RsnzT4gnw@freedb-hzfiz.mongodb.net/test?retryWrites=true').
  then(() => { console.log('connected to mongoose db'); })
  .catch(() => { console.log('database error'); });



io.on('connection', (socket) => {

    socket.on('message', function (data) {
        console.log(moment().format('llll') + ' Got message: ' + data);
        data = JSON.parse(data);
        // if message has room value... it is a message to change alias/room
        if (data.room) {
              //if already in room we leave it
              if (socket.current_room != data.room) {
                  socket.leave(socket.current_room);
                  // join new room
                  socket.current_room = data.room;
                  socket.join(socket.current_room);
               }
              // update alias
              socket.alias = data.alias;
     
              // find messages in room.... get only the top(50/etc) and sort by descending date to get them in order
              Message.find({ room: data.room }, function (err, docs) {

                  for (var i = 0; i < docs.length; i++) {
                      // format the date for easy display...
                      docs[i].date = moment(docs[i].date).format('MMMM Do YYYY, h:mm:ss a');
                  }
                  io.in(socket.current_room).emit('message', { type: 'newChannel', messages: docs });

              }).limit(top_messages).sort({date: -1});
    
        }
        else {
          // this message is a new chat entry....
              data.date = new Date();
              data.room = socket.current_room;
              data.alias = socket.alias;
              var message = new Message({
                alias: data.alias,
                message: data.message,
                date:data.date,
                room: data.room
              });
              message.save();
              data.date = moment(data.date).format('MMMM Do YYYY, h:mm:ss a');
              io.in(socket.current_room).emit('message', data);
        }
    
  })
});


// Initialize our websocket server on port 5000
http.listen(5000, () => {
    console.log('started on port 5000');
});
