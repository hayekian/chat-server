const mongoose = require('mongoose');

const messageSchema = mongoose.Schema({
	alias: {type: String, required: true},
  room: { type: String, required: true },
  date: {type: String, required: true },
	message: {type:String, required: true}});

module.exports = mongoose.model('Message', messageSchema);

